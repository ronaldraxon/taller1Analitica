---
title: "Taller 1 - Consumo de Tarjetas de Crédito"
author: "Ronald Fernando Rodríguez B"
date: "Agosto 7, 2018"
output:
  pdf_document: default
bibliography: references.bib
link-citations: yes
---


```{r setup, include=FALSE, echo=FALSE}
knitr::opts_chunk$set(echo = TRUE)
#paquetes<-c("psych","e1071","readxl","R.utils","dplyr","wordcloud","ggpubr")
#install.packages(paquetes)
#install.packages("ggpubr")
library(R.utils)
library(dplyr)
library(wordcloud)
library(ggplot2)
library(ggpubr)
library(plyr)
library(plotly)
library(psych)
library(tm)
library(dendextend)
```

## Contexto de negocio (Businnes and Data Understanding)
De acuerdo con la información publicada por las entidades bancarias como: como Banco de Bogotá[-@Bbogota], Bancolombia[-@Bcolombia],  Colpatria[-@Bcolpatria], Banco de Occidente[-@Boccidente], se pueden obtener descuentos entre 1% y 70% en compras de 
prendas de vestir, entretenimiento, restaurantes, educación, salud, hotelería y tecnología, entre otros. Este porcentaje varía según el producto,las alianzas entre las razones sociales y los bancos, y las franquicias asociadas a las tarjetas de crédito.

Según la sección financiera del periódico [EL TIEMPO](http://www.eltiempo.com/) y tomando datos de la Superintendencia Financiera [-@SupFinComptarjeta2017][-@SupFinComptarjeta2018], se indica que a mitades de 2017 había alrededor de 14,8 millones de plásticos activos en el país, mientras que el más reciente Reporte de Inclusión Financiera de la Banca de las Oportunidades[-@Boportunidades] registra que poco más de 9 millones de colombianos tienen tarjeta de crédito. "Esto deja descubierto un mercado potencial de unos 30 millones de colombianos al que se puede llegar con la oferta de este medio de pago, en momentos en que la misma banca adelanta una campaña para reducir el uso del efectivo".[@NegocioTarjetaC] 

### Antecedentes
Con el fin de llevar a cabo el análisis, el banco "Dinero Seguro" ha dispuesto el archivo infobanca.xlsx, el cual contiene información de consumo (transacciones) del último mes de 47.871 tarjetahabientes. 
![Descripción de variables.](images/variables1.png)

La base de datos comprende registros de uno de los 10 bancos colombianos más grandes en crédito de consumo, cada uno de los cuales puede tener tarjetas de tres diferentes franquicias (Visa, Diners y una tercera que el banco maneja)

### Objetivos de negocio

A partir del escenario y las oportunidades emergentes, el banco "Dinero Seguro" está interesado en desarrollar promociones para grupos diferentes de clientes de acuerdo a su ocasiones de consumo.

### Criterios de éxito de negocio

El banco está a la espera de un reporte utilizando la metodología CRISP-DM que identifique e ilustre patrones de comportamiento en los tarjetahabientes durante el último mes y liste recomendaciones de negocio,
para orientar el esfuerzo comercial en el banco "Dinero Seguro"

### Metas de minería de datos

1. Identificar los puntos con mayor cantidad de transacciones y mayor monto promedio 
2. Identificar patronesde de comportamiento por día de las transacciones en los puntos con mayor cantidad de transacciones y monto promedio
3. Identificar las concentraciones de transacciones y monto promedio por compras internacionales y nacionales
4. Identificar las concentraciones de transacciones y monto promedio por franquicia
5. Buscar posibles relaciones entre el grupo de cliente y los sitios frecuentes de compra, los montos, días compra, y tipo de compra(nacional e internacional)

### Criterios de éxito de minería de datos

Establecer un perfilamiento de clientes a partir de sitios frecuentes, franquicia utilizada, días de compra y tipo de compra(nacional e internacional)

El repositorio con el código de desarrollo se puede acceder en la siguiente dirección:
<https://gitlab.com/ronaldraxon/taller1Analitica.git>

### Exploración de datos

Estos análisis pretende un acercamiento inicial a los datos, refinar una descripción de los mismos, preparar pasos para su limpieza,  establecer las hipótesis iniciales, entre otros. Se realiza la carga inicial de los datos y visualizacion de los metadatos

```{r importarDatos, eval=TRUE} 
library(readxl)
infoClienteBanca<-read_excel("datasource/infoclientebanca.xlsx")
str(infoClienteBanca)
```
A pesar que no se tiene la información del grupo de cliente, se conservará con el fin de tener puntos de referencia en análisis posteriores. Adicionalmente el campo relacionado con los situis de consumo frecuente tiene un valor importante de negocio, por lo que se realizarán trabajos de limpieza para extraer la mayor cantidad de información posible. 
Los campos relacionados con porcentaje se reemplazarán por valores en pesos al múltiplicarlo con el monto promedio (promedio_por_transaccion) y el número de transacciones. En la exploración, se determinará si se puede utilizar dicho estimador o si es necesario encontrar un valor que permita establecer en pesos la participación de las franquicias, los tipos de compra(nacional e internacional) y los días de la semana. Es importante aclarar que los campos relacionados con los totales de porcentaje (porcentaje_nacional_total y porcentaje_internacional_total) no se tendrán en cuenta, al igual que el campo CLIENTE.

A continuación se visualiza un resumen de los campos con los que se iniciará la exploración

```{r mostrarCampoIniciales, eval=TRUE, echo= FALSE} 
infoClienteBancaExploracion <- infoClienteBanca
infoClienteBancaExploracion$grupo_de_cliente <- as.factor(infoClienteBancaExploracion$grupo_de_cliente)
infoClienteBancaExploracion$Sitio_consumo_masfrecuente <- as.factor(infoClienteBancaExploracion$Sitio_consumo_masfrecuente)
summary(infoClienteBancaExploracion[c(2,3,4,5,6,7,26)])
```
Se evidencia una participación elevada por parte del grupo de cliente A; aspectos iniciales de negocio y de calidad de datos como transacciones minimas de 0 y 1 peso (asumiendo que al ser bancos colombianos la divisa sería en pesos colombianos) que pueden sugerir incongruencia en los datos o transacciones relacionadas con validaciones a cuentas y suscripciones por internet. Adicionalmente, se encuentra un total de 3276 registros de sitios de consumo frecuente con la etiqueta SIN NOMBRE. Puede que estos registros no sean útiles en análisis posteriores y sean candidatos para descartar.Por otra parte, los campos relacionados con los montos tienen valores extremos distanciados con respecto a la media lo que puede sugerir sesgos en cada registro que en si corresponde a un conjunto de transacciones por un cliente durante el último mes(si existen desviaciones estándar mayores al monto promedio de transacciones se podría empezar a corroborar dicho sesgo).

```{r exploracionTx, eval=TRUE, echo= FALSE, fig.height=3,fig.width=10} 
par(mfrow=c(1,3))
plot(log10(infoClienteBancaExploracion$promedio_por_transaccion),ylab="Monto promedio TX - Log10",xlab = "Registro",main="1A")
plot(infoClienteBancaExploracion$Numero_de_transacciones,ylab="Cantidad de TX",xlab = "Registro",main="1B")
plot(infoClienteBancaExploracion$Numero_de_transacciones,log10(infoClienteBancaExploracion$promedio_por_transaccion),ylab="Monto promedio TX - Log10",xlab = "Cantidad de TX",main="1C")
```

Se puede apreciar que la concentración de monto promedio de transacciones (TX) está entre $10^4$ y aproximadamente $10^7$ (Fig. 1A). Por su parte la mayor concentración de cantidad de transacciones se encuentra entre 1 y 60 (Fig. 1B) y se observa quela relación del promedio y el número de transacciones (Fig. 1C) NO es directamente proporcional (entre menos transacciones el promedio de transaccion es mayor en una gran cantidad de casos). Se evidencian algunos atípicos con monto promedio menor que $10^4$ al igual que los registro con una cantidad de transacciones mayor a 100, que debido a su baja frecuencia son cantidatos para descartar.


```{r exploracionTxMinMaxDesv, eval=TRUE, echo= FALSE, fig.height=4,fig.width=10} 
#par(mfrow=c(2,2))
plot(log10(infoClienteBancaExploracion$transaccion_minima),log10(infoClienteBancaExploracion$transaccion_maxima),xlab = "Monto TX Min - Log10",ylab="Monto TX Max - Log10",main="Figura 2 - Monto Max Vs. Monto Mín")
#plot(log10(infoClienteBancaExploracion$transaccion_maxima),log10(infoClienteBancaExploracion$desviacion_estandar_por_transaccion),xlab = "Monto TX Max - Log10",ylab="StdDEV TX - Log10",main="2B")
#plot(infoClienteBancaExploracion$Numero_de_transacciones,log10(infoClienteBancaExploracion$desviacion_estandar_por_transaccion),xlab = "Número de TX",ylab="StdDEV TX - Log10",main="2C")
#plot(log10(infoClienteBancaExploracion$transaccion_minima),log10(infoClienteBancaExploracion$desviacion_estandar_por_transaccion),xlab = "Monto TX Min - Log10",ylab="StdDEV TX - Log10",main="2D")
```

Con en el fin de establecer si los registros con monto de transacciones entre el mínimo y el primer cuartil, implicaban montos máximos inferiores se confrontan las variables (Fig. 2) para corroborarlo o si por el contrario, existe alguna relación entre ellas. Como se puede evidenciar existen un conglomerado importánte en registros con montos mínimos de transacción entre 0 y 3.300 pesos con montos máximos entre 10.000 y cerca de 1'000.000 de pesos. Este comportamiento sugiere a nivel de negocio un segmento de clientes que realizan registros o compra de servicios por internet ya que podría explicar el compotamiento de la alta frecuencia de montos mínimos bajos. Para corroborarlo, se realiza una extracción de dichos registros y se enlistan los 5 sitios de consumo más frecuentes.

```{r sitiosConsumoFrecuenteTXMínimoBajo, eval=TRUE, echo= FALSE}
infoClienteBancaLimpioBajosTXMin<-infoClienteBancaExploracion[infoClienteBancaExploracion$transaccion_minima<=3300,]
conteoSitiosFrecuentes <-count(infoClienteBancaLimpioBajosTXMin,'Sitio_consumo_masfrecuente')
arrange(conteoSitiosFrecuentes, desc(freq))[1:5,]
```

Cómo se pude apreciar existe una cantidad relevante de transacciones orientadas a las compras en internet, supermercados y aerolíneas. Estos sitios se tendrán en cuenta para análisis posteriores. 

Con el fin de comprender el comportamiento de los gripos de clientes, se realiza una exploración del monto maximo en transacciones y el número de transacciones por cada grupo.

```{r montoMaximoGruposClienteHist, eval=TRUE, echo= FALSE, fig.height=5,fig.width=10} 
#puede sugerir atacar promedios de trasacciones hasta 1 millon
infoClienteBancaExploracionClienteA <- infoClienteBancaExploracion[infoClienteBancaExploracion$grupo_de_cliente=="A",]
infoClienteBancaExploracionClienteB <- infoClienteBancaExploracion[infoClienteBancaExploracion$grupo_de_cliente=="B",]
infoClienteBancaExploracionClienteC <- infoClienteBancaExploracion[infoClienteBancaExploracion$grupo_de_cliente=="C",]
infoClienteBancaExploracionClienteD <- infoClienteBancaExploracion[infoClienteBancaExploracion$grupo_de_cliente=="D",]
infoClienteBancaExploracionClienteE <- infoClienteBancaExploracion[infoClienteBancaExploracion$grupo_de_cliente=="E",]

par(mfrow=c(2,5))
hist(log10(infoClienteBancaExploracionClienteA$transaccion_maxima),xlab="TX Max A",main="")
hist(log10(infoClienteBancaExploracionClienteB$transaccion_maxima),xlab="TX Max B",main="")
hist(log10(infoClienteBancaExploracionClienteC$transaccion_maxima),xlab="TX Max C",main="")
hist(log10(infoClienteBancaExploracionClienteD$transaccion_maxima),xlab="TX Max D",main="")
hist(log10(infoClienteBancaExploracionClienteE$transaccion_maxima),xlab="TX Max E",main="")
hist(infoClienteBancaExploracionClienteA$Numero_de_transacciones,xlab="Num. TX A",main="")
hist(infoClienteBancaExploracionClienteB$Numero_de_transacciones,xlab="Num. TX B",main="")
hist(infoClienteBancaExploracionClienteC$Numero_de_transacciones,xlab="Num. TX C",main="")
hist(infoClienteBancaExploracionClienteD$Numero_de_transacciones,xlab="Num. TX D",main="")
hist(infoClienteBancaExploracionClienteE$Numero_de_transacciones,xlab="Num. TX E",main="")
```

Aunque los rangos de montos de transacción son similares para todos los grupos, se puede evidenciar una diferencia de frecuencia de los mismos. Estos grupos probablemente pretender separar segments de alto, medio y bajo consumo.

### Verificación de calidad de datos

a partir del resumen previo se evidencia que los registros no presentan valores ausentes, o que no correspondan a valores no númericos en los campos de promedio y porcentaje. No obstante, en el campo sitio consumo mas frecuente se encuentra un total de 3276 (6% del conjunto de datos) registros con el valor "SIN NOMBRE" por lo que para afectos de perfilamiento y agrupamiento, no se utitilizarán dichos registros.

Por otra parte, existen registros cuya desviaciones estándar es de 0 y con una cantidad de transacciones de 1 para la mayoría de las ocasiones. No obstante, si los casos con una cantidad de transacciones mayor a uno podría sugerir algún tipo de responsabilidad fija (compra de medicamentos por semana) o un hábito de compra de un servicio o producto específico, se realiza la revisión de dichos registros para entender el motivo de la baja variabilidad

```{r desviacionEstCero, eval=TRUE, echo= FALSE} 
registrosDesvesCero<-infoClienteBancaExploracion[infoClienteBancaExploracion$desviacion_estandar_por_transaccion==0,]
registrosDesvesCeroTXMayorAUno <- registrosDesvesCero[registrosDesvesCero$Numero_de_transacciones>1,]
registrosDesvesCeroTXMayorAUno <-registrosDesvesCeroTXMayorAUno[,c("Numero_de_transacciones",
                                                                   "Sitio_consumo_masfrecuente",
                                                                   "promedio_por_transaccion")]
arrange(registrosDesvesCeroTXMayorAUno, desc(Numero_de_transacciones))[1:10,]
```

Los sitios de consumo que se presentan, sugieren una incongruencia en la cantidad de transacciones y obteniendo una desviación estándar de 0. Tomando ejemplos como el servicio de ambulancia con una frecuencia de 9 en un mes, servicios de seguros y comerciantes de ventas telefónicas con 11 y 8 transacciones respectivamente, es muy poco probable por no decir improbable que durante un mes se realice tal candidad de transacciones con exactamente el mismo valor. Si bien no se demuestra la verosimulitud de estos registros, se optará por reemplazar las frecuencias de todos estos registros a 1

### Limpieza de datos

Con el fin de reducir la cantidad de datos atípicos y establecer un conjunto de datos que permita llevar a cabo el ejercicio de agrupamiento y segmentación y extraer la información relevante para las propuestas de negocio, se realizan las siguientes actividades:

1. Remover registros con la etiqueta "SIN NOMBRE" en el campo sitio_consumo_masfrecuente.
```{r removerSitiosSinNombre, eval=TRUE, echo= FALSE} 
infoClienteBancaLimpio<-infoClienteBanca[infoClienteBanca$Sitio_consumo_masfrecuente!="SIN NOMBRE",]
```

2. Extraer registros con transacciones_maxima por encima de 40.000 pesos.
```{r reegistrosTXMayor40000, eval=TRUE, echo= FALSE} 
infoClienteBancaLimpio <- infoClienteBancaLimpio[infoClienteBancaLimpio$transaccion_maxima>=40000,]
```

3. Para reducir el sesgo y la kurtosis se crea el campo promedioPorTransaccionLog, calculando el logaritmo en base 10 del campo de monto promedio de transacciones
```{r reegistrosPromedioTXLog, eval=TRUE, echo= FALSE} 
infoClienteBancaLimpio$promedioPorTransaccionLog <-log10(infoClienteBancaLimpio$promedio_por_transaccion)
describe(infoClienteBancaLimpio$promedioPorTransaccionLog)
```

4. Se reemplaza la cantidad de transacciones en los registros con desviación estándar igual a 0 para que sean de un solo registro
```{r reegistrosDesvEstandar0ConFrecuencia1, eval=TRUE, echo= FALSE} 
infoClienteBancaLimpio[infoClienteBancaLimpio$desviacion_estandar_por_transaccion==0,]$Numero_de_transacciones<-1
```

5. Separar registros con transacciones minimas mayor a 3300, menor igual a 3300 y conformar los conjuntos correspondientes. El subconjunto a utilizar en clústering será aquel con transacciones minimas mayores a 3300
```{r reegistrosSeparadosTXMinima, eval=TRUE, echo= FALSE} 
infoClienteBancaLimpioBajosTXMin<-infoClienteBancaLimpio[infoClienteBancaLimpio$transaccion_minima<=3300,]
infoClienteBancaLimpioAltosTXMin<-infoClienteBancaLimpio[infoClienteBancaLimpio$transaccion_minima>3300,]
```

### Construcción de datos

Con el fin de facilitar la lectura conservando en gran parte la legibiliad del texto para los sitios frecuentes de consuma se realiza un tratamiento, removiendo espacios, guiones, paréntesis entre otros caracteres.

```{r depuracionSitiosFrecuentes, eval=TRUE, echo= FALSE} 
infoClienteBancaLimpioAltosTXMin$sitiosConsumo <- infoClienteBancaLimpioAltosTXMin$Sitio_consumo_masfrecuente
infoClienteBancaLimpioAltosTXMin$sitiosConsumo <- gsub(",", "", infoClienteBancaLimpioAltosTXMin$sitiosConsumo)
infoClienteBancaLimpioAltosTXMin$sitiosConsumo <- gsub("\\(", "", infoClienteBancaLimpioAltosTXMin$sitiosConsumo)
infoClienteBancaLimpioAltosTXMin$sitiosConsumo <- gsub(")", "", infoClienteBancaLimpioAltosTXMin$sitiosConsumo)
infoClienteBancaLimpioAltosTXMin$sitiosConsumo <- gsub("/", "", infoClienteBancaLimpioAltosTXMin$sitiosConsumo)
infoClienteBancaLimpioAltosTXMin$sitiosConsumo <- gsub("-", "", infoClienteBancaLimpioAltosTXMin$sitiosConsumo)
infoClienteBancaLimpioAltosTXMin$sitiosConsumo <- gsub("\\.", "", infoClienteBancaLimpioAltosTXMin$sitiosConsumo)
infoClienteBancaLimpioAltosTXMin$sitiosConsumo <- tolower(infoClienteBancaLimpioAltosTXMin$sitiosConsumo)
infoClienteBancaLimpioAltosTXMin$sitiosConsumo <- toCamelCase(infoClienteBancaLimpioAltosTXMin$sitiosConsumo)
infoClienteBancaLimpioAltosTXMin$sitiosConsumo <- as.factor(infoClienteBancaLimpioAltosTXMin$sitiosConsumo)
```
```{r muestraSitiosFrecuentes, eval=TRUE, echo= TRUE} 
levels(infoClienteBancaLimpioAltosTXMin$sitiosConsumo)[1:10]
```

Se crea el campo montoTotalTX que corresponde a la sumatoria de los valores de una distribución normal que se construye con los 
campos Numero_de_transacciones,	promedio_por_transaccion y desviacion_estandar_por_transaccion

```{r creacionMontoTotalTX, eval=TRUE, echo= TRUE} 
totalDNormal <- function(n, campoCantidadTX, campoMediaTX, campoDesviacionTX)
sum(rnorm(n[campoCantidadTX],n[campoMediaTX],n[campoDesviacionTX]))
parametros <-infoClienteBancaLimpioAltosTXMin[,c("Numero_de_transacciones",
                                                 "promedio_por_transaccion",
                                                 "desviacion_estandar_por_transaccion")]
totales <- apply(parametros,1,totalDNormal,
                 campoCantidadTX ="Numero_de_transacciones",
                 campoMediaTX ="promedio_por_transaccion",
                 campoDesviacionTX = "desviacion_estandar_por_transaccion")
infoClienteBancaLimpioAltosTXMin$montoTotalTX <- totales
```

Ahora con el campo montoTotalTX se asignan los valores para las tarjetas multiplicando el porcentaje de uso por franquicia, tipo de transacción (internacional y nacional), las franjas de día (mañana, tarde y noche) y los días de la semana.

```{r creacionMontosTarjetasDiasYFranjas, eval=TRUE, echo= FALSE}
datos <- infoClienteBancaLimpioAltosTXMin 
datos$montoVisaNacional<-datos$porcentaje_visa_nacional * datos$montoTotalTX
datos$montoVisaInternacional	<-datos$porcentaje_visa_nacional * datos$montoTotalTX
datos$montoMastercardNacional	<-datos$porcentaje_visa_nacional * datos$montoTotalTX
datos$montoMastercardInternacional	<-datos$porcentaje_visa_nacional * datos$montoTotalTX
datos$montoOtraFranquiciaNacional	<-datos$porcentaje_visa_nacional * datos$montoTotalTX
datos$montoOtrafranquiciaInternacional	<-datos$porcentaje_visa_nacional * datos$montoTotalTX
datos$montoNacionalTotal	<- datos$montoVisaNacional + datos$montoMastercardNacional + datos$montoOtraFranquiciaNacional
datos$montoInternacionalTotal	<- datos$montoVisaInternacional + datos$montoMastercardInternacional + datos$montoOtrafranquiciaInternacional
datos$montoManana	<-datos$porcentaje_manana * datos$montoTotalTX
datos$montoTarde	<-datos$porcentaje_tarde * datos$montoTotalTX
datos$montoNoche	<-datos$porcentaje_noche * datos$montoTotalTX
datos$montoDOMINGO	<-datos$porcDOMINGO * datos$montoTotalTX
datos$montoLUNES	<-datos$porcLUNES * datos$montoTotalTX
datos$montoMARTES	<-datos$porcMARTES * datos$montoTotalTX
datos$montoMIERCOLES	<-datos$porcMIERCOLES * datos$montoTotalTX
datos$montoJUEVES	<-datos$porcJUEVES * datos$montoTotalTX
datos$montoVIERNES	<-datos$porcVIERNES * datos$montoTotalTX
datos$montoSABADO<-datos$porcSABADO * datos$montoTotalTX
datos <- datos[,c(2,27:47)]
```

### Formato y descripcion de datos

```{r descripcionDatos, eval=TRUE, echo= FALSE}
str(datos)
```

### Selección de modelo

Se realiza Clústering por días
```{r dendoPorDía, eval=FALSE, echo= TRUE}
datosClusterPorFran <- as.data.frame(datos[,c(3,13:22)])
datosClusterPorFran <-datosClusterPorFran%>%
  group_by(sitiosConsumo)%>%
  summarise(manana = sum(montoManana),
            tarde= sum(montoTarde),
            noche= sum(montoNoche),
            domingo= sum(montoDOMINGO),
            lunes= sum(montoLUNES),
            martes=sum(montoMARTES),
            miercoles=sum(montoMIERCOLES),
            jueves=sum(montoJUEVES),
            viernes=sum(montoVIERNES))
datosClusterPorFran<-as.data.frame(datosClusterPorFran)
etiquetasSitios <- datosClusterPorFran$sitiosConsumo
datosClusterPorFran<-as.data.frame(datosClusterPorFran[,2:10])                        
rownames(datosClusterPorFran)<-etiquetasSitios
datosClusterPorFranScale<-scale(datosClusterPorFran)
heatmap(datosClusterPorFranScale,hclustfun=hclust)
```

Se realiza Clústering por Franquicias 

```{r dendoPorDía2, eval=FALSE, echo= TRUE}
datosClusterPorDias <- as.data.frame(datos[,c(3,5:10,13:22)])
datosClusterPorDias <-datosClusterPorDias%>%
  group_by(sitiosConsumo)%>%
  summarise(manana = sum(montoManana),
            tarde= sum(montoTarde),
            noche= sum(montoNoche),
            visaNac= sum(montoVisaNacional),
            VisaInt= sum(montoVisaInternacional),
            MasterNac=sum(montoMastercardNacional),
            MasterInt=sum(montoMastercardInternacional),
            OtroNac=sum(montoOtraFranquiciaNacional),
            OtroInt=sum(montoOtrafranquiciaInternacional))
datosClusterPorDias<-as.data.frame(datosClusterPorDias)
etiquetasSitios <- datosClusterPorDias$sitiosConsumo
datosClusterPorDias<-as.data.frame(datosClusterPorDias[,2:10])                        
rownames(datosClusterPorDias)<-etiquetasSitios
datosClusterPorDiasScale<-scale(datosClusterPorDias)
heatmap(datosClusterPorDiasScale,hclustfun=hclust)
```


![Dendograma De Días y Franjas.](images/heat1.png)

![Dendograma De Franquicias.](images/heat2.png)

##Referencias