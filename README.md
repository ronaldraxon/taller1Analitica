# Taller 1 - Consumo de Tarjetas de Crédito
## Contexto de negocio (Businnes and Data Understanding)
De acuerdo con la información publicada por las entidades bancarias como: como Banco de Bogotá[-@Bbogota], Bancolombia[-@Bcolombia],  Colpatria[-@Bcolpatria], Banco de Occidente[-@Boccidente], se pueden obtener descuentos entre 1% y 70% en compras de 
prendas de vestir, entretenimiento, restaurantes, educación, salud, hotelería y tecnología, entre otros. Este porcentaje varía según el producto,las alianzas entre las razones sociales y los bancos, y las franquicias asociadas a las tarjetas de crédito.

Según la sección financiera del periódico [EL TIEMPO](http://www.eltiempo.com/) y tomando datos de la Superintendencia Financiera [-@SupFinComptarjeta2017][-@SupFinComptarjeta2018], se indica que a mitades de 2017 había alrededor de 14,8 millones de plásticos activos en el país, mientras que el más reciente Reporte de Inclusión Financiera de la Banca de las Oportunidades[-@Boportunidades] registra que poco más de 9 millones de colombianos tienen tarjeta de crédito. "Esto deja descubierto un mercado potencial de unos 30 millones de colombianos al que se puede llegar con la oferta de este medio de pago, en momentos en que la misma banca adelanta una campaña para reducir el uso del efectivo".[@NegocioTarjetaC] 

### Antecedentes
Con el fin de llevar a cabo el análisis, el banco "Dinero Seguro" ha dispuesto el archivo infobanca.xlsx, el cual contiene información de consumo (transacciones) del último mes de 47.871 tarjetahabientes. 
![Descripción de variables.](images/variables1.png)

La base de datos comprende registros de uno de los 10 bancos colombianos más grandes en crédito de consumo, cada uno de los cuales puede tener tarjetas de tres diferentes franquicias (Visa, Diners y una tercera que el banco maneja)

### Objetivos de negocio

A partir del escenario y las oportunidades emergentes, el banco "Dinero Seguro" está interesado en desarrollar promociones para grupos diferentes de clientes de acuerdo a su ocasiones de consumo.

### Criterios de éxito de negocio

El banco está a la espera de un reporte utilizando la metodología CRISP-DM que identifique e ilustre patrones de comportamiento en los tarjetahabientes durante el último mes y liste recomendaciones de negocio,
para orientar el esfuerzo comercial en el banco "Dinero Seguro"

### Metas de minería de datos

1. Identificar los puntos con mayor cantidad de transacciones y mayor monto promedio 
2. Identificar patronesde de comportamiento por día de las transacciones en los puntos con mayor cantidad de transacciones y monto promedio
3. Identificar las concentraciones de transacciones y monto promedio por compras internacionales y nacionales
4. Identificar las concentraciones de transacciones y monto promedio por franquicia
5. Buscar posibles relaciones entre el grupo de cliente y los sitios frecuentes de compra, los montos, días compra, y tipo de compra(nacional e internacional)

### Criterios de éxito de minería de datos

Establecer un perfilamiento de clientes a partir de sitios frecuentes, franquicia utilizada, días de compra y tipo de compra(nacional e internacional)
